'use strict';

describe('Controller: ActionuploadCtrl', function () {

  // load the controller's module
  beforeEach(module('uploadFileApp'));

  var ActionuploadCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ActionuploadCtrl = $controller('ActionuploadCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ActionuploadCtrl.awesomeThings.length).toBe(3);
  });
});
