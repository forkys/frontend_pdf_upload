'use strict';

describe('Directive: actionUploadDir', function () {

  // load the directive's module
  beforeEach(module('uploadFileApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<action-upload-dir></action-upload-dir>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the actionUploadDir directive');
  }));
});
