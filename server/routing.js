var express = require('express');
var app = express();
var cors = require('cors');
const request = require('request');
var bodyParser = require('body-parser');

app.use(cors());
app.use( bodyParser.json({limit: '100mb'}) );


app.post('/upload-file', function (req, responseApp) {
  const payload = {
    name: req.body.name,
    file: req.body.data
  };
  request.post('http://localhost:9090/elaborate-doc', {
    json: payload
  }, (error, res, body) => {
    if (error) {
      responseApp.send(error);
    }
    responseApp.send(res);
  })
});

app.post('/create-data-set', function (req, responseApp) {
  const payload = {
    name: req.body.name,
    file: req.body.data,
    nameDataSet: req.body.nameDataSet
  };
  request.post('http://localhost:9090/create-data-set', {
    json: payload
  }, (error, res, body) => {
    if (error) {
      responseApp.send(error);
    }
    responseApp.send(res);
  })
});

app.post('/save-rows', function (req, responseApp) {
  request.post('http://localhost:9090/save-rows', {
    json: req.body
  }, (error, res, body) => {
    if (error) {
      responseApp.send(error);
    }
    responseApp.send(res);
  })
});

app.get('/get-parts-pdf', function (req, responseApp) {
  request.get('http://localhost:9090/get-parts-pdf', 
  (error, res, body) => {
    console.log(error);
    console.log(body);
    if (error) {
      responseApp.send(error);
    }
    responseApp.send(body);
  })
});

app.get('/list-classifiers', function (req, responseApp) {
  request.get('http://localhost:9090/list-classifiers', 
  (error, res, body) => {
    console.log(error);
    console.log(body);
    if (error) {
      responseApp.send(error);
    }
    responseApp.send(body);
  })
});

app.post('/change-classifier', function (req, responseApp) {
  request.post('http://localhost:9090/change-classifier', {
    json: req.body
  }, (error, res, body) => {
    if (error) {
      responseApp.send(error);
    }
    responseApp.send(res);
  })
});
  

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});