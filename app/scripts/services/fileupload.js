'use strict';

/**
 * @ngdoc service
 * @name uploadFileApp.fileUpload
 * @description
 * # fileUpload
 * Service in the uploadFileApp.
 */
angular.module('uploadFileApp')
  .service('fileUpload', function () {
    // AngularJS will instantiate a singleton by calling "new" on this function
  });
