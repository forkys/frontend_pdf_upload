'use strict';

/**
 * @ngdoc service
 * @name uploadFileApp.classifiers
 * @description
 * # fileUpload
 * Service in the uploadFileApp.
 */
angular.module('uploadFileApp')
  .service('classifiers', function ($http) {

    var self = this;
    self.values ="";

    self.getList = function(){ 
        $http({
            url: 'http://localhost:3000/list-classifiers',
            method: 'GET'
          }).then(function (response){
            var newValues = [];
            for(var i=0; i < response.data.length; i++){
              newValues.push(response.data[i].split(".")[response.data[i].split(".").length -1]);
            }
            self.values = newValues;
          },function (error){
              alert("internal error!");
        });
    }
    self.getList();

    self.changeClassifier = function(classifier){
      $http({
          url: 'http://localhost:3000/change-classifier',
          method: 'POST',
          data: {newClassifier: classifier}
        }).then(function (response){
          alert("classifier changed"); 
          self.getList();
        },function (error){
          alert("internal error!");
      });
  }
  });
