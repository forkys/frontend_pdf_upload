'use strict';

/**
 * @ngdoc service
 * @name uploadFileApp.partsPDF
 * @description
 * # fileUpload
 * Service in the uploadFileApp.
 */
angular.module('uploadFileApp')
  .service('partsPDF', function ($http) {

    var self = this;
    self.values =[];

    var colors = ["#0053ff", "#00ff5d","#00fdff","#ccff00"]

    self.getList = function(){
        $http({
            url: 'http://localhost:3000/get-parts-pdf',
            method: 'GET'
          }).then(function (response){
            var parts = response.data.split(",");
            for(var i=0; i < parts.length; i++){
                self.values.push({part: parts[i], color: colors[i]});/*
                self.values.push({part: parts[i], color: '#' + Math.floor(Math.random()*16777215).toString(16)});*/
            }
          },function (error){
              alert("internal error!");
        });
    }
    self.getList();

  });
