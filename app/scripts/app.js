'use strict';

/**
 * @ngdoc overview
 * @name uploadFileApp
 * @description
 * # uploadFileApp
 *
 * Main module of the application.
 */
angular
  .module('uploadFileApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'pdfjsViewer',
    'ngMaterial'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .when('/actionUpload', {
        templateUrl: 'views/actionupload.html',
        controller: 'ActionuploadCtrl',
        controllerAs: 'actionUpload'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
