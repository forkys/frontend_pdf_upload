'use strict';

/**
 * @ngdoc function
 * @name uploadFileApp.controller:ActionuploadCtrl
 * @description
 * # ActionuploadCtrl
 * Controller of the uploadFileApp
 */
angular.module('uploadFileApp')
  .controller('ActionuploadCtrl', function ($scope, $sce) {
    
    $scope.$on("showDoc", function (evt, fileURL) {
      debugger;
      if($scope.src_pdf !== undefined){
        $scope.src_pdf = undefined;
      }
      $scope.src_pdf = angular.copy($sce.trustAsResourceUrl(fileURL));
    });
  });
