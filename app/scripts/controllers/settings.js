'use strict';

/**
 * @ngdoc function
 * @name uploadFileApp.controller:SettingsCtrl
 * @description
 * # ActionuploadCtrl
 * Controller of the uploadFileApp
 */
angular.module('uploadFileApp')
  .controller('settingsCtrl', function (classifiers, $scope, $http, partsPDF, $mdDialog, $timeout, $sce) {

    $scope.fileSelected = {};
    var reader = new FileReader();

    
    $scope.$watch(function () { return classifiers.values }, function (newVal, oldVal) {
      if (typeof newVal !== 'undefined') {
        $scope.selected = newVal[0];
        $scope.classifiers = newVal;
      }
    });

    $scope.changeClassifier = function(classifier){
      classifiers.changeClassifier(classifier);
    }

    $scope.uploadHttpFile = function() {

      const byteCharacters = atob(reader.result.replace("data:application/pdf;base64,",""));
      const byteNumbers = new Array(byteCharacters.length);
      for (let i = 0; i < byteCharacters.length; i++) {
          byteNumbers[i] = byteCharacters.charCodeAt(i);
      }
      const byteArray = new Uint8Array(byteNumbers);
      var file = new Blob([byteArray], {type: reader.type});
      var fileURL = URL.createObjectURL(file); 
      
        $http({
          url: 'http://localhost:3000/upload-file',
          method: 'POST',
          data: {name: $scope.fileSelected.name, data: $scope.fileSelected.content}
        }).then(function (response){
            if(response.data.statusCode !== 200){
              alert(response.data.body);
            }else{

              var keys = Object.keys(response.data.body);
              for(var i=0; i < keys.length; i++){
                for(var y=0; y < partsPDF.values.length; y++){
                  if(response.data.body[keys[i]] == partsPDF.values[y].part){
                    response.data.body[keys[i]] = partsPDF.values[y].color;
                    break;
                  }
                }
              }
              if($scope.operation == "test")
                var predictions = response.data.body;
              else  
                var predictions = undefined;
               

              $scope.$broadcast("showLegend", true);              
              $mdDialog.show({
                controller: function($scope, fileURL ,$sce, predictions){
                  $scope.predictions = predictions;
                  $scope.src_pdf = angular.copy($sce.trustAsResourceUrl(fileURL));
                },
                templateUrl: '../../views/actionupload.html',
                parent: angular.element(document.body),
                clickOutsideToClose:true,
                fullscreen: true,
                locals: {
                  fileURL: fileURL,
                  predictions: predictions
                }
              })
              .then(function(answer) {
                $scope.status = 'You said the information was "' + answer + '".';
              }, function() {
                $scope.status = 'You cancelled the dialog.';
              });
            }
            $scope.loaded = false;
          },function (error){
              alert("internal error!");
        });
    };

    $scope.uploadHttpDataset = function() {
      const byteCharacters = atob(reader.result.replace("data:application/pdf;base64,",""));
      const byteNumbers = new Array(byteCharacters.length);
      for (let i = 0; i < byteCharacters.length; i++) {
          byteNumbers[i] = byteCharacters.charCodeAt(i);
      }
      const byteArray = new Uint8Array(byteNumbers);
      var file = new Blob([byteArray], {type: reader.type});
      var fileURL = URL.createObjectURL(file); 
      /* $scope.$broadcast("showDoc", fileURL);*/
      $scope.$broadcast("showLegend", false);
      $mdDialog.show({
        controller: function($scope, fileURL ,$sce){
          $scope.src_pdf = angular.copy($sce.trustAsResourceUrl(fileURL));
        },
        templateUrl: '../../views/actionupload.html',
        parent: angular.element(document.body),
        clickOutsideToClose:true,
        fullscreen: true,
        locals: {
          fileURL: fileURL
        }
      })
      .then(function(answer) {
        $scope.status = 'You said the information was "' + answer + '".';
      }, function() {
        $scope.status = 'You cancelled the dialog.';
      });
      $scope.loaded = false;
    };

    $scope.uploadFile = function(files) {
      var file = files[0];
      $scope.loaded = true;
      reader.addEventListener("loadend", function(evt) {
        $scope.$apply(function () {
          $scope.fileSelected.content = reader.result;
          $scope.fileSelected.name = file.name;
          if($scope.operation == "test"){
            $scope.uploadHttpFile();
          }else{
            $scope.uploadHttpDataset();
          }
        });
      });
      if(reader.type = 'application/pdf') {
        reader.readAsDataURL(files[0]);
      }
    };

  });
