'use strict';

/**
 * @ngdoc function
 * @name uploadFileApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the uploadFileApp
 */
angular.module('uploadFileApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
