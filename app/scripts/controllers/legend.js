'use strict';

/**
 * @ngdoc function
 * @name uploadFileApp.controller:legendCtrl
 * @description
 * # ActionuploadCtrl
 * Controller of the uploadFileApp
 */
angular.module('uploadFileApp')
  .controller('legendCtrl', function ($scope, partsPDF) {

    $scope.$on("showLegend", function (evt, show) {
      $scope.showLegend = show;
    });

    $scope.$watch(function () { return partsPDF.values }, function (newVal, oldVal) {
      if (typeof newVal !== 'undefined') {
        $scope.parts = partsPDF.values;
      }
    });
  });
