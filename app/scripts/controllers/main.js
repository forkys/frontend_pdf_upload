'use strict';

/**
 * @ngdoc function
 * @name uploadFileApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the uploadFileApp
 */
angular.module('uploadFileApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
