'use strict';

/**
 * @ngdoc directive
 * @name uploadFileApp.directive:settingsDir
 * @description
 * # settingsDir
 */
angular.module('uploadFileApp')
  .directive('settingsDir', function () {
    return {
      templateUrl: '../../views/settings.html',
      restrict: 'E',
      controller: 'settingsCtrl'
    };
  });
