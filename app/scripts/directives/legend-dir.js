'use strict';

/**
 * @ngdoc directive
 * @name uploadFileApp.directive:legendDir
 * @description
 * # legendDir
 */
angular.module('uploadFileApp')
  .directive('legendDir', function () {
    return {
      templateUrl: '../../views/legend.html',
      restrict: 'E',
      controller: 'legendCtrl'
    };
  });
