'use strict';

/**
 * @ngdoc directive
 * @name uploadFileApp.directive:actionUploadDir
 * @description
 * # actionUploadDir
 */
angular.module('uploadFileApp')
  .directive('actionUploadDir', function () {
    return {
      templateUrl: '../../views/actionupload.html',
      restrict: 'E',
      scope:true,
      controller: 'ActionuploadCtrl'
    };
  });
